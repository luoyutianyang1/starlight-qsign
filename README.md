# starlight-qsign
![动态访问量](https://count.kjchmc.cn/get/@starlight-qsign?theme=rule34)<br>
[![Gitee](https://img.shields.io/badge/星点签名-1?style=for-the-badge&logo=gitee&color=rgb(218%2C16%2C30))](https://gitee.com/wuliya336/starlight-qsign)[![Github](https://img.shields.io/badge/星点签名-1?style=for-the-badge&logo=github&color=rgb(0%2C0%2C0))](https://github.com/wuliya336/starlight-qsign)<br>
`starlight-qsign`是一个`Yunzai-Bot`的扩展插件，提供公共签名列表，45解决方案功能<br>


---
## 安装与更新

### 使用Git安装（推荐）

请将 `starlight-qsign` 放置在 Yunzai-Bot 的 plugins 目录下，重启 Yunzai-Bot 后即可使用<br>

请使用 git 进行安装，以方便后续升级。在 Yunzai-Bot 根目录夹打开终端，运行下述指令之一<br>

// 使用gitee
```
git clone --depth=1 https://gitee.com/wuliya336/starlight-qsign.git ./plugins/starlight-qsign/
pnpm install --filter=starlight-qsign
```
// 使用github

```
git clone --depth=1 https://github.com/wuliya336/starlight-qsign.git ./plugins/starlight-qsign/
pnpm install --filter=starlight-qsign
```

## 使用帮助
对机器人发送`#API列表`或`45`即可获取本插件所收集的公共签名列表<br>
如需更新发送`#星点签名更新`

## 贡献
如果你也想参与贡献并提供自己所搭建的签名服务,请前往[Gitlab](https://gitlab.com/wuliya336/starlight-qsign)提交并pr<br>
### 如何提交
一. 修改`data/apilist.json`文件,请参考json数组并提交<br>
二. fork仓库修改`data/apilist.json`然后PR

### 手工下载安装（不推荐）

手工下载安装包，解压后将`starlight-qsign-master`更名为`starlight-qsign`，然后放置在Yunzai的plugins目录内<br>

虽然此方式能够使用，不利于后续升级，故不推荐使用<br>

---

# 资源

* [Miao-Yunzai](https://github.com/yoimiya-kokomi/Miao-Yunzai) : 喵版Yunzai [Gitee](https://gitee.com/yoimiya-kokomi/Miao-Yunzai)
  / [Github](https://github.com/yoimiya-kokomi/Miao-Yunzai)
* [Yunzai-V3](https://github.com/yoimiya-kokomi/Yunzai-Bot) ：Yunzai V3 - 喵喵维护版（使用 icqq）
* [Yunzai-V3](https://gitee.com/Le-niao/Yunzai-Bot) ：Yunzai V3 - 乐神原版（使用 oicq）