import common from '../../../lib/common/common.js';
import yaml from 'js-yaml';
import fs from 'fs';

export class Stats extends plugin {
  constructor() {
    super({
      name: 'startlight-qsign',
      event: 'message',
      priority: -33699,
      rule: [
        {
          reg: '^(#)?(API|api)(信息|统计)',
          fnc: 'stats',
        },
      ],
    });
  }

  async send(e) {
    await e.reply('正在获取统计信息，请稍候...', true);
  }

  async stats(e) {
    let todayStats = {};
    let totalStats = {};
    const userAgent = 'statlight-qsign';

    await this.send(e);

    try {
      const configPath = './plugins/starlight-qsign/config/config.yaml';
      const config = yaml.safeLoad(fs.readFileSync(configPath, 'utf8'));
      const statsUrl = config.statsUrl;

      const statsResponse = await fetch(statsUrl, {
        headers: {
          'User-Agent': userAgent,
        },
      });

      if (statsResponse.ok) {
        const statsData = await statsResponse.json();
        todayStats = statsData['今日访问量'];
        totalStats = statsData['总访问量'];
      } else {
        throw new Error('无法获取统计信息');
      }
    } catch (error) {
      console.error('获取统计信息时出错:', error);
      await e.reply('获取统计信息时出错，请稍后重试。');
      return true;
    }

    await new Promise(resolve => setTimeout(resolve, 1000));
    const msg = [
      '签名API统计',
      'API访问量(今日):',
      `8978: ${todayStats['8978'] || 'N/A'}\n8988: ${todayStats['8988'] || 'N/A'}\n8996: ${todayStats['8996'] || 'N/A'}`,
      'API访问量(总计):',
      `8978: ${totalStats['8978'] || 'N/A'}\n8988: ${totalStats['8988'] || 'N/A'}\n8996: ${totalStats['8996'] || 'N/A'}`,
    ];
    await e.reply(common.makeForwardMsg(e, msg, `点击查看签名API统计`));
    return true;
  }
}
