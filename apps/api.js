import common from '../../../lib/common/common.js';
import fs from 'fs';

export class api extends plugin {
  constructor() {
    super({
      name: 'startlight-qsign',
      event: 'message',
      priority: -33699,
      rule: [
        {
          reg: '^(#)?(公共签名|公共签名api|公共api签名|公共API签名|api|签名|公共签名API|API签名|API)?(列表|45)',
          fnc: 'list',
        },
      ],
    });
  }

  async send(e) {
    await e.reply('正在获取公共签名API列表信息,请稍候...', true);
  }

  async list(e) {
    const rawData = fs.readFileSync('./plugins/starlight-qsign/data/apilist.json', 'utf8');
    const providers = JSON.parse(rawData);
    const userAgent = 'starlight-qsign';

    await this.send(e);
    const msg = ['公共签名API列表'];

    for (const provider in providers) {
      const providerInfo = providers[provider];
      msg.push(`由 ${provider} 提供的:`);

      if (provider === '时雨') {
        msg.push('需获取授权，请加群659945190私聊群主');
        providerInfo.forEach((info) => {
          for (const key in info) {
            const url = info[key];
            msg.push(`${key}: \n${url}`);
          }
        });
        continue;
      }

      const fetchPromises = providerInfo.map(async (info) => {
        const fetchResults = [];
        for (const key in info) {
          const url = info[key];
          try {
            const start = Date.now();
            const response = await fetch(url, {
              method: 'GET',
              headers: { 'User-Agent': userAgent },
            });
            const status = response.status === 200 ? '✅ 正常 ' : '❎ 异常 ';
            const delay = response.status === 200 ? `${Date.now() - start}ms` : 'timeout';
            fetchResults.push(`${key}: ${status}${delay}\n${url}`);
          } catch (error) {
            console.error(`获取 ${url} 时出错:`, error);
            fetchResults.push(`${key}: ❎ 异常 timeout\n${url}`);
          }
        }
        return fetchResults.join('\n');
      });

      const results = await Promise.all(fetchPromises);
      msg.push(...results.map(result => result.split('\n').join('\n')));
    }

    await e.reply(common.makeForwardMsg(e, msg, `点击查看公共签名API列表`));
    return true;
  }
}
